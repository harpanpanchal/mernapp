import React from 'react';
import axios from 'axios';
import './App.css';
class App extends React.Component{

  state = {
    title: '',
    body: '',
    posts:[]
  };

  componentDidMount() {
    this.getBlogPost()
  }
  getBlogPost = () => {
    axios.get('/api').then((response) => {
      const data = response.data;
      this.setState({
        posts: data
      })
      console.log("Data has been received");


    }).catch(() => {
      
    })
  }

  handleChange = ({ target}) => {
    const { name, value } = target;

    this.setState({
      [name] : value
    })
  }
  submitForm = (e) => {
    e.preventDefault();

    const payload = {
      title: this.state.title,
      body: this.state.body
    };

    axios({
      url: '/api/save/',
      method: 'post',
      data: payload
    }).then(() => {
      console.log("data has been sent to the server");
      this.resetForm();
      this.getBlogPost();
    }).catch(() => {
      console.log('Internal Server Error')
      
    })
  }
  
  resetForm = () => {
    this.setState({
      title: '',
      body: ''
    });
  };
  displayBlogPost = (posts) => {
    if (!posts.length) return null;
    return posts.map((post, index) => {
      return(<div key={index} className='blog-post__display'>
        <h3>{post.title}</h3>
        <p>{post.body}</p>
      </div>)
      
    })
  }
  render() {
    return (<div className='app'>
      <h2>Welcome to my page</h2>

      <form onSubmit={this.submitForm}>
        <div className='form-input'><input type='text' placeholder='Enter a title' name='title' value={this.state.title} onChange={this.handleChange}  /></div>
        <div className='form-input'><textarea placeholder='Body' name='body' cols='30' rows='10' value={this.state.body} onChange={this.handleChange}>
        </textarea>
        </div>
          <button>Submit</button>
</form>
      <div className="blog-post">{this.displayBlogPost(this.state.posts)}</div>
    </div>)
  }
}

export default App;