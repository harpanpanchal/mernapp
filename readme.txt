Fullstack Mern Application (using React.js / Node.js / Express / MongoDB)

#Installation
1) Refer package.json file in the root directory to install the dependencies and run the project.
a) Run 'npm install' in the terminal in the root folder. This will install all the dependencies for Server.
b) Run 'npm run install-client' in the terminal. This will install all the dependencies in the Client folder (React).
c) Run 'npm run dev'. This will run server and client both at the same time.

d) Run 'http://localhost:8080/api/ to see the records fetched from the MongoDB live server.
e) Run 'http://localhost:3000/' to see the client side (React) page. This page will display the records and as well as allow the user to insert new record by submitting the form. As soon as the data is saved in the database, it will display on the page as a the new record along with the existing records.